using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using System.Linq;
using System.Collections.Generic;

namespace PopoverViewSharp
{
	public class PopoverView :UIView
	{
		#region Constants - Configure look/feel		
		// BOX GEOMETRY

		//Height/width of the actual arrow
		const float kArrowHeight = 12.0f;

		//padding within the box for the contentView
		const float kBoxPadding = 2.0f;

		//control point offset for rounding corners of the main popover box
		const float kCPOffset = 1.8f;

		//radius for the rounded corners of the main popover box
		const float kBoxRadius = 4.0f;

		//Curvature value for the arrow.  Set to 0.0f; to make it linear.
		const float kArrowCurvature = 6.0f;

		//Minimum distance from the side of the arrow to the beginning of curvature for the box
		const float kArrowHorizontalPadding = 5.0f;

		//Alpha value for the shadow behind the PopoverView
		const float kShadowAlpha = 0.4f;

		//Blur for the shadow behind the PopoverView
		const float kShadowBlur = 3.0f;

		//Box gradient bg alpha
		const float kBoxAlpha = 0.95f;

		//Padding along top of screen to allow for any nav/status bars
		const float kTopMargin = 50.0f;

		//margin along the left and right of the box
		const float kHorizontalMargin = 10.0f;

		//padding along top of icons/images
		const float kImageTopPadding = 3.0f;

		//padding along bottom of icons/images
		const float kImageBottomPadding = 3.0f;


		// DIVIDERS BETWEEN VIEWS

		//Bool that turns off/on the dividers
		const bool kShowDividersBetweenViews = false;

		//color for the divider fill
		UIColor kDividerColor = new UIColor(0.329f, 0.341f, 0.353f, 0.15f);


		// BACKGROUND GRADIENT

		//bottom color white in gradient bg
		//UIColor kGradientBottomColor = new UIColor(0.98f, 0.98f, 0.98f, kBoxAlpha);
        UIColor kGradientBottomColor = new UIColor(227.0f/255.0f, 227.0f/255.0f, 223.0f/255.0f, kBoxAlpha);
        //UIColor kGradientBottomColor = UIColor.FromRGB (227, 227, 223);

		//top color white value in gradient bg
		//UIColor kGradientTopColor = new UIColor(1.0f, 1.0f, 1.0f, kBoxAlpha);
        UIColor kGradientTopColor = new UIColor(227.0f/255.0f, 227.0f/255.0f, 223.0f/255.0f, kBoxAlpha);
        //UIColor kGradientTopColor = UIColor.FromRGB (227, 227, 223);

		// TITLE GRADIENT

		//bool that turns off/on title gradient
		const bool kDrawTitleGradient = true; 

		//bottom color white value in title gradient bg
		UIColor kGradientTitleBottomColor = new UIColor(0.93f, 0.93f, 0.93f, kBoxAlpha);

		//top color white value in title gradient bg
		UIColor kGradientTitleTopColor = new UIColor(1.0f, 1.0f, 1.0f, kBoxAlpha);


		// FONTS		
		//normal text font
		UIFont kTextFont = UIFont.FromName(@"HelveticaNeue", 16.0f);

		//normal text color
		UIColor kTextColor = new UIColor(0.329f, 0.341f, 0.353f, 1f);

		//normal text alignment
		const UITextAlignment kTextAlignment = UITextAlignment.Center;

		//title font
		UIFont kTitleFont = UIFont.FromName(@"HelveticaNeue-Bold", 16.0f);

		//title text color
		UIColor kTitleColor = new UIColor(0.329f, 0.341f, 0.353f, 1f);
		#endregion

		UIView TitleView { get; set; }
		UIView ContentView { get; set; }
		string[] SubviewsArray { get; set; }
		IPopoverViewDelegate Delegate { get; set; }


		RectangleF boxFrame;
		SizeF contentSize;
		PointF arrowPoint;
		
		bool above;
		
		//id<PopoverViewDelegate> delegate;
		
		UIView parentView;
		
		UIView topView;
		
		List<UIView> subviewsArray;
		
		List<RectangleF> dividerRects;
		
		UIView contentView;
		
		UIView titleView;
		
		UIActivityIndicatorView activityIndicator;

		//Instance variable that can change at runtime
		bool showDividerRects;

		private PopoverView(RectangleF frame) : base(frame)
		{
			// Initialization code			
			this.BackgroundColor = UIColor.Clear;			

			this.TitleView = null;
			this.ContentView = null;
			
			showDividerRects = kShowDividersBetweenViews;
		}

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, string text, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, text);
			popoverView.Delegate = popoverViewDelegate;
			//popoverView.Release;
			return popoverView;
		}				

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, string title, string text, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, title, text);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}		

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, UIView[] viewArray, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, viewArray);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}	
		
		
		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, string title, UIView[] viewArray, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, title, viewArray);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, string[] stringArray, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, stringArray);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, string title, string[] stringArray, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, title, stringArray);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, string[] stringArray, UIImage[] imageArray, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, stringArray, imageArray);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, string title, string[] stringArray, UIImage[] imageArray, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, title, stringArray, imageArray);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, string title, UIView cView, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, title, cView);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}

		public static PopoverView ShowPopoverAtPoint(PointF point, UIView view, UIView cView, IPopoverViewDelegate popoverViewDelegate)
		{
			var popoverView = new PopoverView(RectangleF.Empty);
			popoverView.ShowAtPoint(point, view, cView);
			popoverView.Delegate = popoverViewDelegate;
			return popoverView;
		}

		void ShowAtPoint(PointF point, UIView view, string text)
		{
			var font = kTextFont;
			
			var textSize = StringSize(text, font, new SizeF(UIScreen.MainScreen.Bounds.Size.Width - kHorizontalMargin * 4.0f, 1000.0f), UILineBreakMode.WordWrap);
			
			var textView = new UILabel(new RectangleF(0f, 0f, textSize.Width, textSize.Height));
			textView.BackgroundColor = UIColor.Clear;
			textView.UserInteractionEnabled = false;
			textView.Lines = 0; //This is so the label word wraps instead of cutting off the text
			textView.Font = font;
			textView.TextAlignment = kTextAlignment;
			textView.TextColor = new UIColor(0.329f, 0.341f, 0.353f, 1f);
			textView.Text = text;
			
			this.ShowAtPoint(point, view, new UIView[] { textView });	
		}

		void ShowAtPoint(PointF point, UIView view, string title, string text)
		{
			var font = kTextFont;

			var textSize = StringSize(text, font, new SizeF(UIScreen.MainScreen.Bounds.Size.Width - kHorizontalMargin * 4.0f, 1000.0f), UILineBreakMode.WordWrap);
						
			var textView = new UILabel(new RectangleF(0f, 0f, textSize.Width, textSize.Height));
			textView.BackgroundColor = UIColor.Clear;
			textView.UserInteractionEnabled = false;
			textView.Lines = 0; //This is so the label word wraps instead of cutting off the text
			textView.Font = font;
			textView.TextAlignment = kTextAlignment;
			textView.TextColor = new UIColor(0.329f, 0.341f, 0.353f, 1f);
			textView.Text = text;
			
			this.ShowAtPoint(point, view, title, new UIView[] { textView });
		}

		void ShowAtPoint(PointF point, UIView view, UIView[] contentViewArray)
		{
			var container = new UIView(RectangleF.Empty);
			
			float totalHeight = 0.0f;
			float totalWidth = 0.0f;
			
			int i = 0;
			
			//Position each view the first time, and identify which view has the largest width that controls
			//the sizing of the popover.
			foreach (var viewC in contentViewArray)
			{				
				viewC.Frame = new RectangleF(0, totalHeight, viewC.Frame.Size.Width, viewC.Frame.Size.Height);
				
				//Only add padding below the view if it's not the last item
				float padding = (i == contentViewArray.Length - 1) ? 0 : kBoxPadding;
				
				totalHeight += viewC.Frame.Size.Height + padding;
				
				if (viewC.Frame.Size.Width > totalWidth)
				{
					totalWidth = viewC.Frame.Size.Width;
				}
				
				container.AddSubview(viewC);
				
				i++;
			}

			//If dividers are enabled, then we allocate the divider rect array.  This will hold NSValues
			if (kShowDividersBetweenViews)
			{
				//dividerRects = new NSMutableArray(viewArray.Length-1);
				dividerRects = new List<RectangleF>(contentViewArray.Length - 1);
			}
			
			container.Frame = new RectangleF(0, 0, totalWidth, totalHeight);
			
			i = 0;
			
			totalHeight = 0;
			
			//Now we actually change the frame element for each subview, and center the views horizontally.
			foreach (var viewC in contentViewArray)
			{
				if (viewC.AutoresizingMask == UIViewAutoresizing.FlexibleWidth)
				{
					//Now make sure all flexible views are the full width
					viewC.Frame = new RectangleF(viewC.Frame.X, viewC.Frame.Y, totalWidth, viewC.Frame.Size.Height);
				} else
				{
					//If the view is not flexible width, then we position it centered in the view
					//without stretching it.
					viewC.Frame = new RectangleF(
						(float)Math.Floor(boxFrame.GetMinX() + totalWidth * 0.5f - viewC.Frame.Size.Width * 0.5f), 
						viewC.Frame.Y, 
						viewC.Frame.Size.Width, 
						viewC.Frame.Size.Height);
				}
				
				//and if dividers are enabled, we record their position for the drawing methods
				if (kShowDividersBetweenViews && i != contentViewArray.Length - 1)
				{
					var dividerRect = new RectangleF(
						viewC.Frame.X, 
						(float)Math.Floor(viewC.Frame.Y + viewC.Frame.Size.Height + kBoxPadding * 0.5f), 
						viewC.Frame.Size.Width, 
						0.5f);

					//dividerRects.

					dividerRects.Add(dividerRect);
				}
				
				//Only add padding below the view if it's not the last item
				float padding = (i == contentViewArray.Length - 1) ? 0.0f : kBoxPadding;
				
				totalHeight += viewC.Frame.Size.Height + padding;
				
				i++;
			}
			
			this.subviewsArray = contentViewArray.ToList();
			
			this.ShowAtPoint(point, view, container);
		}

		void ShowAtPoint(PointF point, UIView view, string title, UIView[] contentViewArray)
		{
			var container = new UIView(RectangleF.Empty);
			
			//Create a label for the title text.
			var titleSize = StringSize(title, kTitleFont);
			var titleLabel = new UILabel(new RectangleF(0.0f, 0.0f, titleSize.Width, titleSize.Height));
			titleLabel.BackgroundColor = UIColor.Clear;
			titleLabel.Font = kTitleFont;
			titleLabel.TextAlignment = UITextAlignment.Center;
			titleLabel.TextColor = kTitleColor;
			titleLabel.Text = title;

			//Make sure that the title's label will have non-zero height.  If it has zero height, then we don't allocate any space
			//for it in the positioning of the views.
			float titleHeightOffset = (titleSize.Height > 0.0f ? kBoxPadding : 0.0f);

			float totalHeight = titleSize.Height + titleHeightOffset + kBoxPadding;
			float totalWidth = titleSize.Width;

			int i = 0;

			//Position each view the first time, and identify which view has the largest width that controls
			//the sizing of the popover.
			foreach (var viewC in contentViewArray)
			{				
				viewC.Frame = new RectangleF(0, totalHeight, viewC.Frame.Size.Width, viewC.Frame.Size.Height);
				
				//Only add padding below the view if it's not the last item.
				float padding = (i == contentViewArray.Length - 1) ? 0.0f : kBoxPadding;
				
				totalHeight += viewC.Frame.Size.Height + padding;
				
				if (viewC.Frame.Size.Width > totalWidth)
				{
					totalWidth = viewC.Frame.Size.Width;
				}
				
				container.AddSubview(viewC);
				
				i++;
			}
			
			//If dividers are enabled, then we allocate the divider rect array.  This will hold NSValues
			if (kShowDividersBetweenViews)
			{
				dividerRects = new List<RectangleF>(contentViewArray.Length - 1);
			}
			
			i = 0;
			
			foreach (var viewC in contentViewArray)
			{
				if (viewC.AutoresizingMask == UIViewAutoresizing.FlexibleWidth)
				{
					//Now make sure all flexible views are the full width
					viewC.Frame = new RectangleF(viewC.Frame.X, viewC.Frame.Y, totalWidth, viewC.Frame.Size.Height);
				} else
				{
					//If the view is not flexible width, then we position it centered in the view
					//without stretching it.
					viewC.Frame = new RectangleF(
						(float)Math.Floor(boxFrame.GetMinX() + totalWidth * 0.5f - viewC.Frame.Size.Width * 0.5f), 
						viewC.Frame.Y, 
						viewC.Frame.Size.Width, 
						viewC.Frame.Size.Height);
				}
				
				//and if dividers are enabled, we record their position for the drawing methods
				if (kShowDividersBetweenViews && i != contentViewArray.Length - 1)
				{
					var dividerRect = new RectangleF(
						viewC.Frame.X, 
						(float)Math.Floor(viewC.Frame.Y + viewC.Frame.Size.Height + kBoxPadding * 0.5f), 
						viewC.Frame.Size.Width,
						0.5f);					

					dividerRects.Add(dividerRect);
				}
				
				i++;
			}
			
			titleLabel.Frame = new RectangleF((float)Math.Floor(totalWidth * 0.5f - titleSize.Width * 0.5f), 0, titleSize.Width, titleSize.Height);
			
			//Store the titleView as an instance variable if it is larger than 0 height (not an empty string)
			if (titleSize.Height > 0)
			{
				this.titleView = titleLabel;
			}
			
			container.AddSubview(titleLabel);
			
			container.Frame = new RectangleF(0, 0, totalWidth, totalHeight);
			
			this.subviewsArray = contentViewArray.ToList();
			
			this.ShowAtPoint(point, view, container);
		}

		void ShowAtPoint(PointF point, UIView view, string[] stringArray)
		{
			// Si el array no funciona bien, puedo probar con un List<T>
			UILabel[] labelArray = new UILabel[stringArray.Length];			
			var font = kTextFont;
			var i = 0;

			foreach (var stringC in stringArray)
			{
				var textSize = StringSize(stringC, font);
				var label = new UILabel(new RectangleF(0, 0, textSize.Width, textSize.Height));
				label.BackgroundColor = UIColor.Clear;
				label.Font = font;
				label.TextAlignment = kTextAlignment;
				label.TextColor = kTextColor;
				label.Text = stringC;
				label.Layer.CornerRadius = 4.0f;
				                        
				labelArray [i] = label;
				i++;
			}
				                        
			this.ShowAtPoint(point, view, labelArray);
		}

		void ShowAtPoint(PointF point, UIView view, string title, string[] stringArray)
		{
			// Si el array no funciona bien, puedo probar con un List<T>
			UILabel[] labelArray = new UILabel[stringArray.Length];			
			var font = kTextFont;
			var i = 0;

			foreach (var stringC in stringArray)
			{
				var textSize = StringSize(stringC, font);
				var label = new UILabel(new RectangleF(0, 0, textSize.Width, textSize.Height));
				label.BackgroundColor = UIColor.Clear;
				label.Font = font;
				label.TextAlignment = kTextAlignment;
				label.TextColor = kTextColor;
				label.Text = stringC;
				label.Layer.CornerRadius = 4.0f;
				                        
				labelArray [i] = label;
				i++;
			}
				                        
			this.ShowAtPoint(point, view, title, labelArray);
		}
					
		void ShowAtPoint(PointF point, UIView view, string[] stringArray, UIImage[] imageArray)
		{
			//Here we do something pretty similar to the stringArray method above.
			//We create an array of subviews that contain the image centered above a label.
			
			if (stringArray.Length != imageArray.Length)
			{
				throw new Exception(@"stringArray.count should equal imageArray.count");
			}
			
			UIView[] tempViewArray = new UIView[stringArray.Length];
			
			var font = kTextFont;
			
			for (int i = 0; i < stringArray.Length; i++)
			{
				var stringC = stringArray [i];				
				
				//First we build a label for the text to set in.
				var textSize = StringSize(stringC, font);
				var label = new UILabel(new RectangleF(0, 0, textSize.Width, textSize.Height));
				label.BackgroundColor = UIColor.Clear;
				label.Font = font;
				label.TextAlignment = kTextAlignment;
				label.TextColor = kTextColor;
				label.Text = stringC;
				label.Layer.CornerRadius = 4.0f;
				                        
				//Now we grab the image at the same index in the imageArray, and create
				//a UIImageView for it.
				var image = imageArray [i];
				var imageView = new UIImageView(image);
				                        
				//Take the larger of the two widths as the width for the container
				float containerWidth = Math.Max(imageView.Frame.Size.Width, label.Frame.Size.Width);
				float containerHeight = label.Frame.Size.Height + kImageTopPadding + kImageBottomPadding + imageView.Frame.Size.Height;
				                        
				//This container will hold both the image and the label
				var containerView = new UIView(new RectangleF(0, 0, containerWidth, containerHeight));
				                        
				//Now we do the frame manipulations to put the imageView on top of the label, both centered
				imageView.Frame = new RectangleF(
					(float)Math.Floor(containerWidth * 0.5f - imageView.Frame.Size.Width * 0.5f), 
					kImageTopPadding, 
					imageView.Frame.Size.Width, 
					imageView.Frame.Size.Height);

				label.Frame = new RectangleF(
					(float)Math.Floor(containerWidth * 0.5f - label.Frame.Size.Width * 0.5f), 
					imageView.Frame.Size.Height + kImageBottomPadding + kImageTopPadding, 
					label.Frame.Size.Width, 
					label.Frame.Size.Height);
				                        
				containerView.AddSubview(imageView);
				containerView.AddSubview(label);
				                        
				label.Dispose();
				imageView.Dispose();
				                        
				tempViewArray [i] = containerView;
				containerView.Dispose();
			}
				                        
			this.ShowAtPoint(point, view, tempViewArray);
		}
	
		void ShowAtPoint(PointF point, UIView view, string title, string[] stringArray, UIImage[] imageArray)
		{
			//This method is identical to the one above except for the last line where it calls
			//the appropriate show...withTitle:withViewArray: selector
			
			if (stringArray.Length != imageArray.Length)
			{
				throw new Exception(@"stringArray.count should equal imageArray.count");
			}
			
			UIView[] tempViewArray = new UIView[stringArray.Length];
			
			var font = kTextFont;
			
			for (int i = 0; i < stringArray.Length; i++)
			{
				var stringC = stringArray [i];
				
				
				//First we build a label for the text to set in.
				var textSize = StringSize(stringC, font);
				var label = new UILabel(new RectangleF(0, 0, textSize.Width, textSize.Height));
				label.BackgroundColor = UIColor.Clear;
				label.Font = font;
				label.TextAlignment = kTextAlignment;
				label.TextColor = kTextColor;
				label.Text = stringC;
				label.Layer.CornerRadius = 4.0f;
				                        
				//Now we grab the image at the same index in the imageArray, and create
				//a UIImageView for it.
				var image = imageArray [i];
				var imageView = new UIImageView(image);
				                        
				//Take the larger of the two widths as the width for the container
				float containerWidth = Math.Max(imageView.Frame.Size.Width, label.Frame.Size.Width);
				float containerHeight = label.Frame.Size.Height + kImageTopPadding + kImageBottomPadding + imageView.Frame.Size.Height;
				                        
				//This container will hold both the image and the label
				var containerView = new UIView(new RectangleF(0, 0, containerWidth, containerHeight));
				                               
				//Now we do the frame manipulations to put the imageView on top of the label, both centered
				imageView.Frame = new RectangleF((float)Math.Floor(containerWidth * 0.5f - imageView.Frame.Size.Width * 0.5f), kImageTopPadding, imageView.Frame.Size.Width, imageView.Frame.Size.Height);
				label.Frame = new RectangleF((float)Math.Floor(containerWidth * 0.5f - label.Frame.Size.Width * 0.5f), imageView.Frame.Size.Height + kImageBottomPadding + kImageTopPadding, label.Frame.Size.Width, label.Frame.Size.Height);
				                               
				containerView.AddSubview(imageView);
				containerView.AddSubview(label);
				                               
				label.Dispose();
				imageView.Dispose();
				                               
				tempViewArray [i] = containerView;
				containerView.Dispose();
			}
				                               
			this.ShowAtPoint(point, view, title, tempViewArray);
		}

		void ShowAtPoint(PointF point, UIView view, string title, UIView contentView)
		{
			this.ShowAtPoint(point, view, title, new UIView[] {contentView});
		}

		void ShowAtPoint(PointF point, UIView view, UIView contentView)
		{
			//NSLog(@"point:%f,%f", point.x, point.y);
			
			this.contentView = contentView;
			
			parentView = view;
			
			//Locate the view at the top fo the view stack.
			var window = UIApplication.SharedApplication.KeyWindow;
			if (window == null)
			{
				window = UIApplication.SharedApplication.Windows [0];
			}

			topView = window.Subviews [0];
			
			var topPoint = topView.ConvertPointFromView(point, view);
			
			arrowPoint = topPoint;
			
			//NSLog(@"arrowPoint:%f,%f", arrowPoint.x, arrowPoint.y);
			
			var topViewBounds = topView.Bounds;
			
			float contentHeight = contentView.Frame.Size.Height;
			float contentWidth = contentView.Frame.Size.Width;
			
			float padding = kBoxPadding;
			
			float boxHeight = contentHeight + 2.0f * padding;
			float boxWidth = contentWidth + 2.0f * padding;
			
			float xOrigin = 0.0f;
			
			//Make sure the arrow point is within the drawable bounds for the popover.
			if (arrowPoint.X + kArrowHeight > topViewBounds.Size.Width - kHorizontalMargin - kBoxRadius - kArrowHorizontalPadding)
			{//Too far to the right
				arrowPoint.X = topViewBounds.Size.Width - kHorizontalMargin - kBoxRadius - kArrowHorizontalPadding - kArrowHeight;
				//NSLog(@"Correcting Arrow Point because it's too far to the right");
			} else if (arrowPoint.X - kArrowHeight < kHorizontalMargin + kBoxRadius + kArrowHorizontalPadding)
			{//Too far to the left
				arrowPoint.X = kHorizontalMargin + kArrowHeight + kBoxRadius + kArrowHorizontalPadding;
				//NSLog(@"Correcting Arrow Point because it's too far to the left");
			}
			
			//NSLog(@"arrowPoint:%f,%f", arrowPoint.x, arrowPoint.y);
			
			xOrigin = (float)Math.Floor(arrowPoint.X - boxWidth * 0.5f);
			
			//Check to see if the centered xOrigin value puts the box outside of the normal range.
			if (xOrigin < topViewBounds.GetMinX() + kHorizontalMargin)
			{
				xOrigin = topViewBounds.GetMinX() + kHorizontalMargin;
			} else if (xOrigin + boxWidth > topViewBounds.GetMaxX() - kHorizontalMargin)
			{
				//Check to see if the positioning puts the box out of the window towards the left
				xOrigin = topViewBounds.GetMaxX() - kHorizontalMargin - boxWidth;
			}
			
			float arrowHeight = kArrowHeight;
			
			float topPadding = kTopMargin;
			
			above = true;
			
			if (topPoint.Y - contentHeight - arrowHeight - topPadding < topViewBounds.GetMinY())
			{
				//Position below because it won't fit above.
				above = false;
				
				boxFrame = new RectangleF(xOrigin, arrowPoint.Y + arrowHeight, boxWidth, boxHeight);
			} else
			{
				//Position above.
				above = true;
				
				boxFrame = new RectangleF(xOrigin, arrowPoint.Y - arrowHeight - boxHeight, boxWidth, boxHeight);
			}
			
			//NSLog(@"boxFrame:(%f,%f,%f,%f)", boxFrame.X, boxFrame.Y, boxFrame.Size.Width, boxFrame.Size.Height);
			
			var contentFrame = new RectangleF(boxFrame.X + padding, boxFrame.Y + padding, contentWidth, contentHeight);
			contentView.Frame = contentFrame;
			this.AddSubview(contentView);
			
			//We set the anchorPoint here so the popover will "grow" out of the arrowPoint specified by the user.
			//You have to set the anchorPoint before setting the frame, because the anchorPoint property will
			//implicitly set the frame for the view, which we do not want.
			this.Layer.AnchorPoint = new PointF(arrowPoint.X / topViewBounds.Size.Width, arrowPoint.Y / topViewBounds.Size.Height);
			this.Frame = topViewBounds;
			this.SetNeedsDisplay();
			
			topView.AddSubview(this);
			
			//Add a tap gesture recognizer to the large invisible view (self), which will detect taps anywhere on the screen.
			var tap = new UITapGestureRecognizer();//this, new MonoTouch.ObjCRuntime.Selector("ViewTapSelector"));

			tap.CancelsTouchesInView = false; // Allow touches through to a UITableView or other touchable view, as suggested by Dimajp.
			tap.AddTarget(() => OnViewTapped(tap));
			this.AddGestureRecognizer(tap);
			//tap.Dispose();
			                                     
			this.UserInteractionEnabled = true;
			                                     
			//Make the view small and transparent before animation
			this.Alpha = 0.0f;
			this.Transform = CGAffineTransform.MakeScale(0.1f, 0.1f);
			                                     
			//animate into full size
			//First stage animates to 1.05x normal size, then second stage animates back down to 1x size.
			//This two-stage animation creates a little "pop" on open.
			UIView.Animate(0.2f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
				this.Alpha = 1.0f;
				this.Transform = CGAffineTransform.MakeScale(1.05f, 1.05f);
			}, () => {
				UIView.Animate(0.08f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
					this.Transform = CGAffineTransform.MakeIdentity();
				}, null);
			});
		}

		void LayoutAtPoint(PointF point, UIView view)
		{
			this.Alpha = 0.0f;
			
			var topPoint = topView.ConvertPointFromView(point, view);
			
			arrowPoint = topPoint;
			
			//NSLog(@"arrowPoint:%f,%f", arrowPoint.x, arrowPoint.y);
			
			var topViewBounds = topView.Bounds;
			
			float contentHeight = contentView.Frame.Size.Height;
			float contentWidth = contentView.Frame.Size.Width;
			
			float padding = kBoxPadding;
			
			float boxHeight = contentHeight + 2.0f * padding;
			float boxWidth = contentWidth + 2.0f * padding;
			
			float xOrigin = 0.0f;
			
			//Make sure the arrow point is within the drawable bounds for the popover.
			if (arrowPoint.X + kArrowHeight > topViewBounds.Size.Width - kHorizontalMargin - kBoxRadius - kArrowHorizontalPadding)
			{//Too far to the right
				arrowPoint.X = topViewBounds.Size.Width - kHorizontalMargin - kBoxRadius - kArrowHorizontalPadding - kArrowHeight;
				//NSLog(@"Correcting Arrow Point because it's too far to the right");
			} else if (arrowPoint.X - kArrowHeight < kHorizontalMargin + kBoxRadius + kArrowHorizontalPadding)
			{//Too far to the left
				arrowPoint.X = kHorizontalMargin + kArrowHeight + kBoxRadius + kArrowHorizontalPadding;
				//NSLog(@"Correcting Arrow Point because it's too far to the left");
			}
			
			//NSLog(@"arrowPoint:%f,%f", arrowPoint.x, arrowPoint.y);
			
			xOrigin = (float)Math.Floor(arrowPoint.X - boxWidth * 0.5f);
			
			//Check to see if the centered xOrigin value puts the box outside of the normal range.
			if (xOrigin < topViewBounds.GetMinX() + kHorizontalMargin)
			{
				xOrigin = topViewBounds.GetMinX() + kHorizontalMargin;
			} else if (xOrigin + boxWidth > topViewBounds.GetMaxX() - kHorizontalMargin)
			{
				//Check to see if the positioning puts the box out of the window towards the left
				xOrigin = topViewBounds.GetMaxX() - kHorizontalMargin - boxWidth;
			}
			
			float arrowHeight = kArrowHeight;
			
			float topPadding = kTopMargin;
			
			above = true;
			
			if (topPoint.Y - contentHeight - arrowHeight - topPadding < topViewBounds.GetMinY())
			{
				//Position below because it won't fit above.
				above = false;
				
				boxFrame = new RectangleF(xOrigin, arrowPoint.Y + arrowHeight, boxWidth, boxHeight);
			} else
			{
				//Position above.
				above = true;
				
				boxFrame = new RectangleF(xOrigin, arrowPoint.Y - arrowHeight - boxHeight, boxWidth, boxHeight);
			}
			
			//NSLog(@"boxFrame:(%f,%f,%f,%f)", boxFrame.X, boxFrame.Y, boxFrame.Size.Width, boxFrame.Size.Height);
			
			var contentFrame = new RectangleF(boxFrame.X + padding, boxFrame.Y + padding, contentWidth, contentHeight);
			contentView.Frame = contentFrame;
			
			//We set the anchorPoint here so the popover will "grow" out of the arrowPoint specified by the user.
			//You have to set the anchorPoint before setting the frame, because the anchorPoint property will
			//implicitly set the frame for the view, which we do not want.
			this.Layer.AnchorPoint = new PointF(arrowPoint.X / topViewBounds.Size.Width, arrowPoint.Y / topViewBounds.Size.Height);
			this.Frame = topViewBounds;
			this.SetNeedsDisplay();
			
			this.UserInteractionEnabled = true;
			
			//animate into full size
			//First stage animates to 1.05x normal size, then second stage animates back down to 1x size.
			//This two-stage animation creates a little "pop" on open.
			UIView.Animate(0.2f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
				this.Alpha = 1.0f;
			}, null);
		}

		void ShowActivityIndicatorWithMessage(string msg)
		{
			if (titleView is UILabel)
			{
				((UILabel)titleView).Text = msg;
			}

			if (subviewsArray != null && subviewsArray.Any())
			{
				UIView.Animate(0.2f, () => {
					foreach (var view in subviewsArray)
					{
						view.Alpha = 0.0f;
					}
				});
				
				if (showDividerRects)
				{
					showDividerRects = false;
					this.SetNeedsDisplay();
				}
			}
			
			if (activityIndicator != null)
			{
				activityIndicator.RemoveFromSuperview();
				activityIndicator.Dispose();
				activityIndicator = null;
			}
			
			activityIndicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			activityIndicator.Frame = new RectangleF(contentView.Bounds.GetMidX() - 10.0f, contentView.Bounds.GetMidY() - 10.0f + 20.0f, 20.0f, 20.0f);
			contentView.AddSubview(activityIndicator);
			
			activityIndicator.StartAnimating();	
		}
		
		void HideActivityIndicatorWithMessage(string msg)
		{
			if (titleView is UILabel)
			{
				((UILabel)titleView).Text = msg;
			}
			
			activityIndicator.StopAnimating();

			UIView.Animate(0.1f, () => {
				activityIndicator.Alpha = 0.0f;
			}, () => {
				activityIndicator.RemoveFromSuperview();
				activityIndicator.Dispose();
				activityIndicator = null;
			});
		}

		void ShowImage(UIImage image, string msg)
		{
			var imageView = new UIImageView(image);
			imageView.Alpha = 0.0f;
			imageView.Frame = new RectangleF(
				(float)Math.Floor(contentView.Bounds.GetMidX() - image.Size.Width * 0.5f), 
				(float)Math.Floor(contentView.Bounds.GetMidY() - image.Size.Height * 0.5f + ((this.titleView != null) ? 20 : 0.0f)), 
				image.Size.Width, 
				image.Size.Height);
			imageView.Transform = CGAffineTransform.MakeScale(0.1f, 0.1f);
			
			contentView.AddSubview(imageView);
			
			if (subviewsArray != null && subviewsArray.Any())
			{
				UIView.Animate(0.2f, () => {
					foreach (var view in subviewsArray)
					{
						view.Alpha = 0.0f;
					}
				});
				
				if (showDividerRects)
				{
					showDividerRects = false;
					this.SetNeedsDisplay();
				}
			}
				
			if (!string.IsNullOrEmpty(msg))
			{
				if (titleView is UILabel)
				{
					((UILabel)titleView).Text = msg;
				}
			}
				
			UIView.Animate(0.2f, 0.2f, UIViewAnimationOptions.CurveEaseInOut, () => {
				imageView.Alpha = 1.0f;
				imageView.Transform = CGAffineTransform.MakeIdentity();
			}, null);
		}
		
		void ShowError()
		{
			var imageView = new UIImageView();//UIImage. imageNamed:@"error"]];
			imageView.Alpha = 0.0f;
			imageView.Frame = new RectangleF(contentView.Bounds.GetMidX() - 20.0f, contentView.Bounds.GetMidY() - 20.0f + ((this.titleView != null) ? 20 : 0.0f), 40.0f, 40.0f);
			imageView.Transform = CGAffineTransform.MakeScale(0.1f, 0.1f);
			                                
			contentView.AddSubview(imageView);
			                                
			if (subviewsArray != null && subviewsArray.Any())
			{
				UIView.Animate(0.1f, () => {
					foreach (var view in subviewsArray)
					{
						view.Alpha = 0.0f;
					}
				});
				
				if (showDividerRects)
				{
					showDividerRects = false;
					this.SetNeedsDisplay();
				}
			}
				
			UIView.Animate(0.1f, 0.1f, UIViewAnimationOptions.CurveEaseInOut, () => {
				imageView.Alpha = 1.0f;
				imageView.Transform = CGAffineTransform.MakeIdentity();
			}, null);
		}
		
		void ShowSuccess()
		{
			var imageView = new UIImageView();// [UIImage imageNamed:@"success"]];
			imageView.Alpha = 0.0f;
			imageView.Frame = new RectangleF(contentView.Bounds.GetMidX() - 20.0f, contentView.Bounds.GetMidY() - 20.0f + ((this.titleView != null) ? 20 : 0.0f), 40.0f, 40.0f);
			imageView.Transform = CGAffineTransform.MakeScale(0.1f, 0.1f);
			                                
			contentView.AddSubview(imageView);
			                                
			if (subviewsArray != null && subviewsArray.Any())
			{
				UIView.Animate(0.1f, () => {
					foreach (var view in subviewsArray)
					{
						view.Alpha = 0.0f;
					}
				});
				
				if (showDividerRects)
				{
					showDividerRects = false;
					this.SetNeedsDisplay();
				}
			}
				
			UIView.Animate(0.1f, 0.1f, UIViewAnimationOptions.CurveEaseInOut, () => {
				imageView.Alpha = 1.0f;
				imageView.Transform = CGAffineTransform.MakeIdentity();
			}, null);
		}	

		//[Export("ViewTapSelector")]
		protected void OnViewTapped(UIGestureRecognizer tap)
        {
            var point = tap.LocationInView(contentView);
			
            //NSLog(@"point:(%f,%f)", point.x, point.y);
			
            var found = false;		

            //for(int i = 0; i < subviewsArray.Count && !found; i++) 
            if (subviewsArray != null)
            {
                //NSLog(@"subviewsArray:%@", subviewsArray);
                var i = 0;

                foreach (var view in subviewsArray)
                {
                    //var view = subviewsArray[i];
			
                    //NSLog(@"Rect:(%f,%f,%f,%f)", view.Frame.X, view.Frame.Y, view.Frame.Size.Width, view.Frame.Size.Height);


                    if (view.Frame.Contains(point))
                    {
                        //The tap was within this view, so we notify the delegate, and break the loop.
					
                        found = true;
					
                        //NSLog(@"Tapped subview:%d", i);
					
                        if (view is UILabel)
                        {
                            var label = (UILabel)view;
                            label.BackgroundColor = new UIColor(0.329f, 0.341f, 0.353f, 1f);
                            label.TextColor = UIColor.White;
                        }

                        // if(this.delegate && [this.delegate respondsToSelector:@selector(PopoverViewdidSelectItemAtIndex:)]) {
                        if (Delegate != null)
                        {
                            Delegate.PopoverViewDidSelectItemAtIndex(this, i);
                        }

                        break;
                    }

                    i++;
                }
            }
			
			if (!found && contentView.Bounds.Contains(point))
			{
				found = true;
				//NSLog(@"popover box contains point, ignoring user input");
			}
			
			if (!found)
			{
				this.Dismiss();
			}
		}

		public void AnimateRotationToNewPoint(PointF point, UIView view, double duration)
		{
			this.LayoutAtPoint(point, view);
		}

		void Dismiss()
		{
			UIView.Animate(0.3f, () => {
				this.Alpha = 0.1f;
				this.Transform = CGAffineTransform.MakeScale(0.1f, 0.1f);
			}, () => {
				this.RemoveFromSuperview();

				if (Delegate != null)
				{
					Delegate.PopoverViewDidDismiss(this);
				}

//				if(this.delegate && [this.delegate respondsToSelector:@selector(popoverViewDidDismiss:)]) {
//					[delegate popoverViewDidDismiss:self];
//				}
			});
		}

		#region Drawing Routines
		public override void Draw(RectangleF rect)
		{
			// Drawing code
			Console.WriteLine("Custom draw");
			
			// Build the popover path
			var frame = boxFrame;
			
			float xMin = frame.GetMinX();
			float yMin = frame.GetMinY();
			
			float xMax = frame.GetMaxX();
			float yMax = frame.GetMaxY();
			
			float radius = kBoxRadius; //Radius of the curvature.
			
			float cpOffset = kCPOffset; //Control Point Offset.  Modifies how "curved" the corners are.
			
			
			/*
     LT2            RT1
     LT1⌜⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⌝RT2
     |               |
     |    popover    |
     |               |
     LB2⌞_______________⌟RB1
     LB1           RB2
     
     Traverse rectangle in clockwise order, starting at LT1
     L = Left
     R = Right
     T = Top
     B = Bottom
     1,2 = order of traversal for any given corner
     
     */
			
			var popoverPath = UIBezierPath.Create();
			popoverPath.MoveTo(new PointF(frame.GetMinX(), frame.GetMinY() + radius));//LT1
			popoverPath.AddCurveToPoint(new PointF(xMin + radius, yMin), new PointF(xMin, yMin + radius - cpOffset), new PointF(xMin + radius - cpOffset, yMin));//LT2
			
			//If the popover is positioned below (!above) the arrowPoint, then we know that the arrow must be on the top of the popover.
			//In this case, the arrow is located between LT2 and RT1
			if (!above)
			{
				popoverPath.AddLineTo(new PointF(arrowPoint.X - kArrowHeight, yMin));//left side
				popoverPath.AddCurveToPoint(arrowPoint, new PointF(arrowPoint.X - kArrowHeight + kArrowCurvature, yMin), arrowPoint);//actual arrow point
				popoverPath.AddCurveToPoint(new PointF(arrowPoint.X + kArrowHeight, yMin), arrowPoint, new PointF(arrowPoint.X + kArrowHeight - kArrowCurvature, yMin));//right side
			}
			
			popoverPath.AddLineTo(new PointF(xMax - radius, yMin));//RT1
			popoverPath.AddCurveToPoint(new PointF(xMax, yMin + radius), new PointF(xMax - radius + cpOffset, yMin), new PointF(xMax, yMin + radius - cpOffset));//RT2
			popoverPath.AddLineTo(new PointF(xMax, yMax - radius));//RB1
			popoverPath.AddCurveToPoint(new PointF(xMax - radius, yMax), new PointF(xMax, yMax - radius + cpOffset), new PointF(xMax - radius + cpOffset, yMax));//RB2
			
			//If the popover is positioned above the arrowPoint, then we know that the arrow must be on the bottom of the popover.
			//In this case, the arrow is located somewhere between LB1 and RB2
			if (above)
			{
				popoverPath.AddLineTo(new PointF(arrowPoint.X + kArrowHeight, yMax));//right side
				popoverPath.AddCurveToPoint(arrowPoint, new PointF(arrowPoint.X + kArrowHeight - kArrowCurvature, yMax), arrowPoint);//arrow point
				popoverPath.AddCurveToPoint(new PointF(arrowPoint.X - kArrowHeight, yMax), arrowPoint, new PointF(arrowPoint.X - kArrowHeight + kArrowCurvature, yMax));
			}
			
			popoverPath.AddLineTo(new PointF(xMin + radius, yMax));//LB1
			popoverPath.AddCurveToPoint(new PointF(xMin, yMax - radius), new PointF(xMin + radius - cpOffset, yMax), new PointF(xMin, yMax - radius + cpOffset));//LB2
			popoverPath.ClosePath();
			
			//// General Declarations
			var colorSpace = CGColorSpace.CreateDeviceRGB();
			var context = UIGraphics.GetCurrentContext();
			
			//// Shadow Declarations
			var shadow = new UIColor(0.0f, 0.0f, 0.0f, kShadowAlpha);
			var shadowOffset = new SizeF(0, 1);
			var shadowBlurRadius = kShadowBlur;
			
			//// Gradient Declarations
			CGColor[] gradientColors = new CGColor[] { kGradientTopColor.CGColor, kGradientBottomColor.CGColor };
			float[] gradientLocations = new float[] {0, 1};
			var gradient = new CGGradient(colorSpace, gradientColors, gradientLocations);
			
			
			//These floats are the top and bottom offsets for the gradient drawing so the drawing includes the arrows.
			float bottomOffset = (above ? kArrowHeight : 0.0f);
			float topOffset = (!above ? kArrowHeight : 0.0f);
			
			//Draw the actual gradient and shadow.
			context.SaveState();
			context.SetShadowWithColor(shadowOffset, shadowBlurRadius, shadow.CGColor);
			context.BeginTransparencyLayer();
			popoverPath.AddClip();
			context.DrawLinearGradient(
				gradient, 
				new PointF(frame.GetMidX(), frame.GetMinY() - topOffset), 
				new PointF(frame.GetMidX(), frame.GetMaxY() + bottomOffset), 
				0);
			context.EndTransparencyLayer();
			context.RestoreState();
			
			//// Cleanup
			gradient.Dispose();
			colorSpace.Dispose();			
			
			//Draw the title background
			if (kDrawTitleGradient)
			{
				//Calculate the height of the title bg
				float titleBGHeight = -1;
				
				//NSLog(@"titleView:%@", titleView);
				
				if (titleView != null)
				{
					titleBGHeight = kBoxPadding * 2.0f + titleView.Frame.Size.Height;
				}
				
				
				//Draw the title bg height, but only if we need to.
				if (titleBGHeight > 0.0f)
				{
					var startingPoint = new PointF(xMin, yMin + titleBGHeight);
					var endingPoint = new PointF(xMax, yMin + titleBGHeight);
					
					var titleBGPath = UIBezierPath.Create();
					titleBGPath.MoveTo(startingPoint);
					titleBGPath.AddLineTo(new PointF(frame.GetMinX(), frame.GetMinY() + radius));//LT1
					titleBGPath.AddCurveToPoint(new PointF(xMin + radius, yMin), new PointF(xMin, yMin + radius - cpOffset), new PointF(xMin + radius - cpOffset, yMin));//LT2
					                            
					//If the popover is positioned below (!above) the arrowPoint, then we know that the arrow must be on the top of the popover.
					//In this case, the arrow is located between LT2 and RT1
					if (!above)
					{
						titleBGPath.AddLineTo(new PointF(arrowPoint.X - kArrowHeight, yMin));//left side
						titleBGPath.AddCurveToPoint(arrowPoint, new PointF(arrowPoint.X - kArrowHeight + kArrowCurvature, yMin), arrowPoint);//actual arrow point
						titleBGPath.AddCurveToPoint(new PointF(arrowPoint.X + kArrowHeight, yMin), arrowPoint, new PointF(arrowPoint.X + kArrowHeight - kArrowCurvature, yMin));//right side
					}
						                            
					titleBGPath.AddLineTo(new PointF(xMax - radius, yMin));//RT1
					titleBGPath.AddCurveToPoint(new PointF(xMax, yMin + radius), new PointF(xMax - radius + cpOffset, yMin), new PointF(xMax, yMin + radius - cpOffset));//RT2
					titleBGPath.AddLineTo(endingPoint);
					titleBGPath.AddLineTo(startingPoint);
					titleBGPath.ClosePath();
						                           
					//// General Declarations
					colorSpace = CGColorSpace.CreateDeviceRGB();
					context = UIGraphics.GetCurrentContext();
						                           
					//// Gradient Declarations
					gradientColors = new CGColor[] { kGradientTitleTopColor.CGColor, kGradientTitleBottomColor.CGColor };
					gradientLocations = new float[] {0, 1};
					gradient = new CGGradient(colorSpace, gradientColors, gradientLocations);

//					NSArray* gradientColors = [NSArray arrayWithObjects:
//						                           (id)kGradientTitleTopColor.CGColor,
//						                           (id)kGradientTitleBottomColor.CGColor, nil];
//						                           CGFloat gradientLocations[] = {0, 1};
//						CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)gradientColors, gradientLocations);
						
						
					//These floats are the top and bottom offsets for the gradient drawing so the drawing includes the arrows.
					topOffset = (!above ? kArrowHeight : 0.0f);
						
					//Draw the actual gradient and shadow.
					context.SaveState();
					context.BeginTransparencyLayer();
					titleBGPath.AddClip();
					context.DrawLinearGradient(gradient, new PointF(frame.GetMidX(), frame.GetMinY() - topOffset), new PointF(frame.GetMidX(), frame.GetMinY() + titleBGHeight), 0);
					context.EndTransparencyLayer();
					context.RestoreState();
						
					var dividerLine = UIBezierPath.FromRect(new RectangleF(startingPoint.X, startingPoint.Y, (endingPoint.X - startingPoint.X), 0.5f));
					new UIColor(0.741f, 0.741f, 0.741f, 0.5f).SetFill();
					dividerLine.Fill();
						
					//// Cleanup
					gradient.Dispose();
					colorSpace.Dispose();
				}
			}
						
						
						
			//Draw the divider rects if we need to
			{
				if (kShowDividersBetweenViews && showDividerRects)
				{
					if (dividerRects != null && dividerRects.Any())
					{
						foreach (var value in dividerRects)
						{
							var rectC = value;
							rectC.X += contentView.Frame.X;
							rectC.Y += contentView.Frame.Y;
										
							var dividerPath = UIBezierPath.FromRect(rectC);
							kDividerColor.SetFill();
							dividerPath.Fill();
						}
					}
				}
			}
		}
		#endregion
	}

}

